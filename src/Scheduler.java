import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private List<Server> servers;
    private Strategy strategy;

    public Scheduler(int Q){
        servers=new ArrayList<>();
        strategy=new Strategy();
        for(int i=0;i<Q;i++){
            Server s=new Server(i+1);
            servers.add(s);
        }
    }

    public void dispatchClient(Client client){
        strategy.addClient(servers,client);
    }

    public boolean emptyServers(){
        boolean empty=true;
        for (Server s: servers) {
            if(!s.isEmpty()){
                empty=false;
            }
        }
        return empty;
    }

    public List<Server> getServers(){
        return servers;
    }
}
