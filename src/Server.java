import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Server  implements Runnable{
    private BlockingQueue<Client> clients;
    private AtomicInteger waitingPeriod;
    private AtomicBoolean exit = new AtomicBoolean(false);
    private int id;
    private int sumWaitTime;

    public Server(int id) {
        this.clients= new LinkedBlockingQueue<>();
        this.waitingPeriod=new AtomicInteger(0);
        this.id = id;
        this.sumWaitTime=0;
        this.exit.set(false);
    }

    public void addClient(Client newClient){
        try {
            clients.put(newClient);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (true){
            int currentWP=waitingPeriod.get();
            int newWP=currentWP+newClient.gettService();
            if(waitingPeriod.compareAndSet(currentWP,newWP)){
                break;
            }
        }
        sumWaitTime+=newClient.gettWait();
    }

    public void decreaseWaitingPeriod(){
        while (true) {
            int currentWP = waitingPeriod.get();
            int newWP = currentWP - 1;
            if (waitingPeriod.compareAndSet(currentWP, newWP))
                break;
        }
    }

    private void trySleep(){
        try {
            Thread.sleep(1000, 2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stop(){
        exit.set(true);
    }

    public void run() {////
        exit.set(false);
        while (!exit.get()) {
            if (!clients.isEmpty()) {
                try {
                    while (clients.element().gettService() > 1) {
                        clients.element().decreaseTService();
                        decreaseWaitingPeriod();
                        trySleep();
                    }
                    decreaseWaitingPeriod();
                    clients.take();
                    trySleep();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else
                trySleep();
        }
    }

    public int getWaitingPeriod() {
        return waitingPeriod.intValue();
    }

    public Client[] getClienst(){
        Client[] clientVector=new Client[]{};
        clients.toArray(clientVector);
        return clientVector;
    }
    
    public boolean isEmpty(){
        return clients.isEmpty();
    }

    public int getSumWaitTime() {
        return sumWaitTime;
    }

    @Override
    public String toString() {
        StringBuilder aux= new StringBuilder();
        for (Client c:clients) {
            aux.append(c.toString()).append(" ");
        }
        return "Server" +id + ": " + (clients.isEmpty() ? "Closed " + waitingPeriod + ";" :  aux + " " + waitingPeriod + ";");
    }
}
