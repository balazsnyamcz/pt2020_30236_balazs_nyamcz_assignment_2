public class Client /*implements Comparable<Client>*/{
    private Integer id;
    private int tArrival;
    private int tService;

    private int tWait;

    public Client(int tArrival, int tService) {
        this.id = 0;
        this.tArrival = tArrival;
        this.tService = tService;
        this.tWait = 0;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int gettArrival() {
        return tArrival;
    }

    public int gettService() {
        return tService;
    }

    public void decreaseTService(){
        this.tService=this.tService-1;
    }

    public int gettWait() {
        return tWait;
    }

    public void settWait(int tWait) {
        this.tWait = tWait;
    }

    @Override
    public String toString() {
        return "("+id +","+ tArrival + "," + tService +","+ tWait +");";
    }
}
