import java.util.List;


public class Strategy {
    public void addClient(List<Server> serverList, Client client){
        Server bestServer=new Server(0);
        int bestWaitingtime=Integer.MAX_VALUE;
        for (Server s:serverList) {
            if(s.getWaitingPeriod() < bestWaitingtime) {
                bestServer = s;
                bestWaitingtime = s.getWaitingPeriod();
            }
        }
        client.settWait(bestWaitingtime+client.gettService());
        bestServer.addClient(client);
    }
}
