import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class SimulationManager implements Runnable{

    public int N;
    public int Q;
    public int tMaxSimulation;
    public int tMinArrival;
    public int tMaxArrival;
    public int tMinService;
    public int tMaxService;
    private Scheduler scheduler;
    private List<Client> generatedClients;
    private PrintWriter output;
    private AtomicBoolean exit = new AtomicBoolean(false);

    public SimulationManager(String inputfileName,String outputfileName){
        readFromTxt(inputfileName);
        generatedClients=new ArrayList<>();
        generateNRandomClients();
        scheduler=new Scheduler(Q);
        output=returnOutput(outputfileName);
        exit.set(false);
        for (Server s: this.scheduler.getServers()) {
            Thread t=new Thread(s);
            t.start();
        }
    }

    public void stop(){
        exit.set(true);
    }

    private void generateNRandomClients(){
        for(int i=0;i<this.N;i++){
            Random random=new Random();
            int arrival=random.nextInt((tMaxArrival-tMinArrival)+1);
            int service=random.nextInt((tMaxService-tMinService)+1);
            generatedClients.add(new Client(arrival+tMinArrival,service+tMinService));
        }
       generatedClients.sort(Comparator.comparing(Client::gettArrival));
        int i=1;
        for (Client c: generatedClients) {
            c.setId(i++);
        }
    }

    public void readFromTxt(String fileName){
        File file=new File(fileName);
        Scanner input;
        try {
            input = new Scanner(file);
            String aux; //Folosesc, pentru verificarea daca la intrare am doua intregi separate cu ','
            int index; // indexul ',' in aux
            this.N=input.nextInt();
            this.Q=input.nextInt();
            this.tMaxSimulation=input.nextInt();
            aux= input.next();
            index=aux.indexOf(',');
            this.tMinArrival=Integer.parseInt(aux.substring(0,index)); //iau substringul inainte lui ',' si transform in int
            this.tMaxArrival=Integer.parseInt(aux.substring(index+1)); //iau substringul dupa ',' si transform in int
            aux= input.next();
            index=aux.indexOf(',');
            this.tMinService=Integer.parseInt(aux.substring(0,index));
            this.tMaxService=Integer.parseInt(aux.substring(index+1));
            input.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public PrintWriter returnOutput(String filename){
        File outputFile=new File(filename);
        PrintWriter output = null;
        try {
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }
            output = new PrintWriter(outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;
    }

    public void writeToTxt(int currentTime){
        output.append("Time ").append(String.valueOf(currentTime));
        output.append("\n"+"Waiting clients: ");
        for (Client c:generatedClients)
            output.append(c.toString()).append(" ");
        for (Server s: scheduler.getServers())
            output.append("\n").append(s.toString());
        output.append("\n"+"\n");
        output.flush();
    }

    @Override
    public String toString() {
        return  "N=" + N +
                ", Q=" + Q +
                "\ntMaxSimulation=" + tMaxSimulation +
                "\ntMinArrival=" + tMinArrival +
                "\ntMaxArrival=" + tMaxArrival +
                "\ntMinService=" + tMinService +
                "\ntMaxService=" + tMaxService ;
    }

    @Override
    public void run() {
        exit.set(false);
        while(!exit.get()) {
            int currentTime = 0;
            while (currentTime < tMaxSimulation && (!generatedClients.isEmpty() || !scheduler.emptyServers())) {
                while (!generatedClients.isEmpty() && currentTime >= generatedClients.get(0).gettArrival()) {
                    scheduler.dispatchClient(generatedClients.remove(0));
                }
                writeToTxt(currentTime);
                currentTime++;
                try {
                    Thread.sleep(1000, 1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            writeToTxt(currentTime);
            float avgWaitTime = 0;
            for (Server s : scheduler.getServers()) {
                avgWaitTime = avgWaitTime + s.getSumWaitTime();
                for (Client c : s.getClienst()) {
                    avgWaitTime = avgWaitTime + c.gettWait();
                }
                s.stop();
            }
            avgWaitTime = avgWaitTime / N;
            System.out.println("Average waiting time: " + avgWaitTime);
            output.append("Average waiting time: ").append(String.valueOf(avgWaitTime));
            output.flush();
        }
    }

    public static void main(String[] args){
        SimulationManager program=new SimulationManager(args[0],args[1]);
        Thread t=new Thread(program);
        t.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        program.stop();
        if(!t.isAlive())
            program.output.close();
    }
}
